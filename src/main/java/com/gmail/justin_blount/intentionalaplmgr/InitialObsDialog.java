/*
 * 2 feb 2014
 * Author: Justin Blount
 * Contact at justin.blount@gmail.com
 */
package com.gmail.justin_blount.intentionalaplmgr;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justinblount
 */
public class InitialObsDialog extends javax.swing.JDialog {
    IntentAgent p;

    // if a row above this is selected, (set unspec) button is disabled, otherwise is enabled
    int max_required_obs = 0;
    String filePathDomain, filePathHistory;
    String currentFlag;
    List<String> Cmd;
    boolean obsLegal = true;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ApplyCWA1;
    private javax.swing.JButton Close;
    private javax.swing.JTable InitialObsTable;
    private javax.swing.JButton SetFalse1;
    private javax.swing.JButton SetTrue1;
    private javax.swing.JButton jButton1;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    /**
     * Creates new form InitialObsDialog
     */
    public InitialObsDialog(IntentAgent parent, boolean modal) {
        super(parent, "Initial Observations", modal);
        p = parent;
        System.out.println("Initial observations dialog...");
        initComponents();

        this.jLabel2.setText("Initial Observations of fluents");

        int r = 0;
        if (p.currstep == 0 && !p.backFromIterateClicked) {
            for (int k = 0; k < p.physical_Fluents.size(); k++) {
                InitialObsTable.getModel().setValueAt(p.physical_Fluents.get(k), r, 0);
                r++;
                max_required_obs++;
            }
        } else {
            System.out.println("backclicked?? size " + p.History.get(p.currstep).observed_size());
            p.backFromIterateClicked = false;

            for (int k = 0; k < p.History.get(p.currstep).observed_size(); k++) {
                InitialObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedName(k), r, 0);
                InitialObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedValue(k), r, 1);
                r++;
                max_required_obs++;
            }

            // after loading table from history, the history is cleared. those that were added by setObserved
            // it will be reloaded with new obs in parseTable
            p.History.get(p.currstep).clearObsHpd();
            p.History.get(p.currstep).clear_obs_hpd();
        }

        filePathDomain = p.Domain.getAbsolutePath();
        filePathHistory = p.HistoryFile.getAbsolutePath();

        // initiall max_name is ir, later it will be getMaxName()
        currentFlag = "\"-c currstep=" + p.getCurrString()
            + ",n=" + p.getCurrString()
            + ",max_name=" + p.getIRString() + "\"";
        Cmd = List.of(
                "crmodels2",
                "--cputime",
                "30",
                "--cr2opts",
                "\"--min-card\"",
                "--gopts",
                currentFlag,
                "1",
                filePathDomain,
                filePathHistory
        );

        System.out.println("legal initial obs command: ");
        for (String s : Cmd)
            System.out.print(s + " ");
        System.out.println();
    }

    /**
     * checks to ensure that the obs made by the user are legal
     * i.e. they are consistent.
     * if they are not legal, the user must change them until they are legal.
     * If the observations are legal then the number of unobserved exogenous
     * actions is parsed from answer set along with the observations and
     * both are stored in the history at currstep.
     */
    private void LegalInitialObs() throws IOException {
        System.out.println("checking initial observations...");
        obsLegal = true;
        InputStream is;
        Runtime rt;
        ProcessBuilder pb;
        Process po;
        BufferedReader br;
        rt = Runtime.getRuntime();

        p.setlabel("Interpreting Observations ... Please wait");

        System.out.println("computing an answer set...");
        pb = new ProcessBuilder(Cmd);
        po = pb.start();
        try {
            po.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("computation ended.");
        is = po.getInputStream();
        br = new BufferedReader(new InputStreamReader(is));

        String s, s2, s3;
        String[] stringTokens;
        int linelength;
        int k;
        s = br.readLine();

        if (s == null || s.startsWith("False")) {
            System.out.println("observations are NOT legal.");
            obsLegal = false;
        } else {
            System.out.println("observations are legal.");
            while (true) {

                s = br.readLine();
                // System.out.println("s "+s);
                if (s == null) {
                    // System.out.println("no models");
                    break;
                } else {
                    stringTokens = s.split(" ");
                    linelength = stringTokens.length;
                    for (k = 0; k < linelength; k++) {
                        s2 = stringTokens[k];
                        // number_unobserved
                        if (s2.startsWith("number_unobserved(")) {
                            p.History.get(p.currstep).setnum_unob(IntentAgent.extractArg1(s2));
                            // System.out.println("number_unob "+s3);
                            System.out.println("extracting number of unobserved occurrences of exogenous actions...");
                            System.out.println("extraction complete - "
                                + p.History.get(p.currstep).getnum_unob() + " unobserved occurrences.");
                        }
                    }
                }
            }
            br.close();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jInternalFrame1 = new javax.swing.JInternalFrame();
        Close = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        InitialObsTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        SetTrue1 = new javax.swing.JButton();
        SetFalse1 = new javax.swing.JButton();
        ApplyCWA1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jInternalFrame1.setVisible(true);

        Close.setText("Finished making Observations");
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });

        InitialObsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object[][]{
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String[]{
                "Name", "Observed value"
            }
        ));
        jScrollPane1.setViewportView(InitialObsTable);

        jLabel2.setText("Initial observations of values of fluents");

        SetTrue1.setText("Set True");
        SetTrue1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetTrue1ActionPerformed(evt);
            }
        });

        SetFalse1.setText("Set False");
        SetFalse1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetFalse1ActionPerformed(evt);
            }
        });

        ApplyCWA1.setText("Apply CWA");
        ApplyCWA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ApplyCWA1ActionPerformed(evt);
            }
        });

        jButton1.setText("Set Unspecified");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel4Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(SetTrue1)
                    .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel4Layout.createSequentialGroup()
                            .add(9, 9, 9)
                            .add(ApplyCWA1))
                        .add(jPanel4Layout.createSequentialGroup()
                            .add(3, 3, 3)
                            .add(SetFalse1)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jButton1)))
                    .add(0, 12, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel4Layout.createSequentialGroup()
                    .add(18, 18, 18)
                    .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(SetTrue1)
                        .add(SetFalse1)
                        .add(jButton1))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(ApplyCWA1))
        );

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel3Layout.createSequentialGroup()
                    .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3Layout.createSequentialGroup()
                            .add(59, 59, 59)
                            .add(jLabel2)))
                    .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel3Layout.createSequentialGroup()
                    .add(13, 13, 13)
                    .add(jLabel2)
                    .add(18, 18, 18)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 280, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jInternalFrame1Layout = new org.jdesktop.layout.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jInternalFrame1Layout.createSequentialGroup()
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .add(124, 124, 124)
                            .add(Close))
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .addContainerGap()
                            .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(25, Short.MAX_VALUE))
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                    .add(16, 16, 16)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                    .add(Close, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(27, 27, 27))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jInternalFrame1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .add(14, 14, 14)
                    .add(jInternalFrame1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed

        // these are new observations, adds to ObsTable
        parseInitTable();
        try {
            // creates history that includes what new obs
            p.printHistoryFile();
            LegalInitialObs();
            if (obsLegal) {
                // calls solver with cmd on history file
                // return true if consistent false otherwis

                // close window ... initial obs are legal
                setVisible(false);
                System.out.println("Initial observations dialog closed.");
            } else {
                p.History.get(p.currstep).clear_obs_hpd();
                p.printHistoryFile();
                System.out.println("history updated with observations.");

                JOptionPane.showMessageDialog(jInternalFrame1,
                    "Observations are not legal (i.e. they make the history inconsistent). "
                        + "Modify your observations ");
            }

            // updates user interface with new observations.
            p.updateHistoryView();
            // begins with determine_ia
            p.AgentLoop();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(InitialObsDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_CloseActionPerformed

    private void SetTrue1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetTrue1ActionPerformed
        int[] sl = InitialObsTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < max_required_obs; i++) {
            InitialObsTable.setValueAt("true", sl[i], 1);
        }
    }//GEN-LAST:event_SetTrue1ActionPerformed

    private void SetFalse1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetFalse1ActionPerformed
        int[] sl = InitialObsTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < max_required_obs; i++) {
            InitialObsTable.setValueAt("false", sl[i], 1);
        }
    }//GEN-LAST:event_SetFalse1ActionPerformed

    private void ApplyCWA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ApplyCWA1ActionPerformed
        int i = 0;
        while (InitialObsTable.getModel().getValueAt(i, 0) != null) {
            if (InitialObsTable.getModel().getValueAt(i, 1) != "true")
                InitialObsTable.getModel().setValueAt("false", i, 1);
            i++;
        }
    }//GEN-LAST:event_ApplyCWA1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int[] sl = InitialObsTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < max_required_obs; i++) {
            InitialObsTable.setValueAt(null, sl[i], 1);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * parse the HpdTable and update hpd Arraylist in history at currstep
     */
    private void parseInitTable() {
        String name, value = "";
        // int k;
        // // parse the ObsTable and update obs Arraylist in history at currstep
        // k = 0;
        // while (InitialObsTable.getModel().getValueAt(k, 0) != null) {
        //     name = (String) InitialObsTable.getModel().getValueAt(k, 0);
        //     value = (String) InitialObsTable.getModel().getValueAt(k, 1);
        //     // System.out.println(name);
        //     if (name != null && value != null) {
        //         p.History.get(p.currstep).setobs(name, value);
        //         p.History.get(p.currstep).addObservedNameValue(name, value);
        //     }
        //     k++;
        // }

        for (int j = 0; j < p.physical_Fluents.size(); j++) {
            name = (String) InitialObsTable.getModel().getValueAt(j, 0);
            value = (String) InitialObsTable.getModel().getValueAt(j, 1);
            // System.out.println(name);
            if (name != null) {
                p.History.get(p.currstep).addObservedNameValue(name, value);
                if (value != null) {
                    p.History.get(p.currstep).setobs(name, value);
                }
            }
        }
    }

    // /**
    //  * @param args the command line arguments
    //  */
    // public static void main(String args[]) {
    //     // Set the Nimbus look and feel
    //     //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    //     // If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
    //     // For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
    //     try {
    //         for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
    //             if ("Nimbus".equals(info.getName())) {
    //                 javax.swing.UIManager.setLookAndFeel(info.getClassName());
    //                 break;
    //             }
    //         }
    //     } catch (ClassNotFoundException ex) {
    //         java.util.logging.Logger.getLogger(InitialObsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (InstantiationException ex) {
    //         java.util.logging.Logger.getLogger(InitialObsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (IllegalAccessException ex) {
    //         java.util.logging.Logger.getLogger(InitialObsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (javax.swing.UnsupportedLookAndFeelException ex) {
    //         java.util.logging.Logger.getLogger(InitialObsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     }
    //     //</editor-fold>
    //
    //     // Create and display the dialog
    //     java.awt.EventQueue.invokeLater(new Runnable() {
    //         public void run() {
    //             InitialObsDialog dialog = new InitialObsDialog(new javax.swing.JFrame(), true);
    //             dialog.addWindowListener(new java.awt.event.WindowAdapter() {
    //                 @Override
    //                 public void windowClosing(java.awt.event.WindowEvent e) {
    //                     System.exit(0);
    //                 }
    //             });
    //             dialog.setVisible(true);
    //         }
    //     });
    // }
}
