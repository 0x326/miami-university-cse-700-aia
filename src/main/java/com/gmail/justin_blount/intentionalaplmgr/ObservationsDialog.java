/*
 * 2 feb 2014
 * Author: Justin Blount
 * Contact at justin.blount@gmail.com
 *
 *
 */
package com.gmail.justin_blount.intentionalaplmgr;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justinblount
 */
public class ObservationsDialog extends javax.swing.JDialog {
    IntentAgent p;
    // denote the last line of the Obs,Hpd table that must be specified,
    int max_required_hpd = 0;
    // int first_special=0;
    // if a row above this is selected, (set unspec) button is disabled, otherwise is enabled
    int max_required_obs = 0;
    // is the row to setValue at
    int rObs = 0;
    int rHpd = 0;

    String filePathDomain, filePathHistory;
    String currentFlag;
    List<String> Cmd;
    boolean obsLegal = true;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ApplyCWA;
    private javax.swing.JButton ApplyCWA1;
    private javax.swing.JButton Close;
    private javax.swing.JTable HpdTable;
    private javax.swing.JTable ObsTable;
    private javax.swing.JButton SetFalse;
    private javax.swing.JButton SetFalse1;
    private javax.swing.JButton SetTrue;
    private javax.swing.JButton SetTrue1;
    private javax.swing.JButton SetUnspec;
    private javax.swing.JButton SetUnspec2;
    private javax.swing.JButton backFromObs;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    // End of variables declaration//GEN-END:variables

    /**
     * Creates new form ObservationsDialog
     */
    public ObservationsDialog(IntentAgent parent, boolean modal) {
        super(parent, "Observations", modal);

        p = parent;
        // System.out.println("obs_dialog");
        initComponents();
        System.out.println("Observations dialog...");

        this.jLabel1.setText("Observations of occurrences of actions at step " + (p.currstep - 1));
        this.jLabel2.setText("Observations of values of fluents at step " + p.currstep);

        if (p.currstep > 0 && !p.backFromIterateClicked) {

            if (p.History.get(p.currstep - 1).getattmpt() != null) {
                // System.out.println("attempted action at "+ p.currstep + p.History.get(p.currstep-1).getattmpt());
                // System.out.println("attempted action at arg1 "+ IntentAgent.extractActionFromAttempt(p.History.get(p.currstep-1).getattmpt()));
                HpdTable.getModel().setValueAt(IntentAgent.extractActionFromAttempt(p.History.get(p.currstep - 1).getattmpt()), rHpd, 0);
                HpdTable.getModel().setValueAt("true", rHpd, 1);
                HpdTable.getModel().setValueAt("true", rHpd, 2);
                rHpd++;
                max_required_hpd++;
            }
            // System.out.println("after obs attmp");
            if (p.History.get(p.currstep).get_obs_goal_size() > 0) {
                // d obs goal
                for (int k = 0; k < p.History.get(p.currstep).get_obs_goal_size(); k++) {
                    ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_goal(k), rObs, 0);
                    ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_goal_expected_value(k), rObs, 1);
                    ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_goal_expected_value(k), rObs, 2);
                    rObs++;
                    max_required_obs++;
                }
            }
            // System.out.println("after obs goal");
            // System.out.println(p.History.get(p.currstep).get_obs_spec_exog_action_size());
            if (p.History.get(p.currstep).get_obs_spec_exog_action_size() > 0) {
                // d obs spec exog
                for (int k = 0; k < p.History.get(p.currstep).get_obs_spec_exog_action_size(); k++) {
                    HpdTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_spec_exog_action(k), rHpd, 0);
                    HpdTable.getModel().setValueAt("false", rHpd, 2);
                    // first_special = rHpd;
                    rHpd++;
                    max_required_hpd++;
                }
            }
            // put selectable goals here too, may become active if hpd(select)
            // System.out.println(p.History.get(p.currstep).get_obs_spec_exog_action_size());
            if (p.History.get(p.currstep).get_obs_spec_exog_action_size() > 0) {
                // d obs spec exog
                for (int k = 0; k < p.History.get(p.currstep).get_obs_spec_exog_action_size(); k++) {
                    if (p.History.get(p.currstep).get_obs_spec_exog_action(k).startsWith("select(")) {
                        ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_selectable_goal(k), rObs, 0);
                        // System.out.println(p.History.get(p.currstep).get_obs_selectable_goal(k));
                        ObsTable.getModel().setValueAt(null, rObs, 1);
                        // exog are never expected to occur
                        ObsTable.getModel().setValueAt("false", rObs, 2);
                        rObs++;
                        // only required if selected  will
                        max_required_obs++;
                    }
                }
            }
            // System.out.println("after selectable goals");
            // exog action must be added to the end
            for (int k = 0; k < p.physical_exogenous_Actions.size(); k++) {
                HpdTable.getModel().setValueAt(p.physical_exogenous_Actions.get(k), rHpd, 0);
                HpdTable.getModel().setValueAt(null, rHpd, 1);
                // exog never expected to occur
                HpdTable.getModel().setValueAt("false", rHpd, 2);
                // System.out.print(rHpd+" ");
                rHpd++;
            }
            // physical fluents
            for (int k = 0; k < p.History.get(p.currstep).get_obs_fluent_size(); k++) {
                ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_fluent(k), rObs, 0);
                ObsTable.getModel().setValueAt(null, rObs, 1);
                ObsTable.getModel().setValueAt(p.History.get(p.currstep).get_obs_fluent_expected_value(k), rObs, 2);
                // System.out.print(rObs+" ");
                rObs++;
            }
            p.History.get(p.currstep).set_rObs(rObs);
            p.History.get(p.currstep).set_rHpd(rHpd);
            p.History.get(p.currstep).set_max_required_obs(max_required_obs);
            p.History.get(p.currstep).set_max_required_hpd(max_required_hpd);
        } else {

            System.out.println("backclicked?? " + p.backFromIterateClicked);
            p.backFromIterateClicked = false;
            // InitialObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedName(k),r,0);
            // InitialObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedValue(k),r,1);

            rObs = p.History.get(p.currstep).get_rObs();
            // System.out.println("back rObs "+rObs);
            rHpd = p.History.get(p.currstep).get_rHpd();
            // System.out.println("back rHpd "+rHpd);
            max_required_obs = p.History.get(p.currstep).get_max_required_obs();
            max_required_hpd = p.History.get(p.currstep).get_max_required_hpd();

            for (int k = 0; k < p.History.get(p.currstep).happened_size(); k++) {
                HpdTable.getModel().setValueAt(p.History.get(p.currstep).getHappenedName(k), k, 0);
                HpdTable.getModel().setValueAt(p.History.get(p.currstep).getHappenedValue(k), k, 1);
                HpdTable.getModel().setValueAt(p.History.get(p.currstep).getHappenedExpected(k), k, 2);
            }

            // System.out.println("after obs attmp");
            for (int k = 0; k < p.History.get(p.currstep).observed_size(); k++) {
                ObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedName(k), k, 0);
                ObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedValue(k), k, 1);
                ObsTable.getModel().setValueAt(p.History.get(p.currstep).getObservedExpected(k), k, 2);
            }

            // clears previous obs and hpd, will be reloaded in ParseTable below
            p.History.get(p.currstep).clearObsHpd();
            p.History.get(p.currstep).clear_obs_hpd();
        }

        // added for legal obs checking
        filePathDomain = p.Domain.getAbsolutePath();
        filePathHistory = p.HistoryFile.getAbsolutePath();

        currentFlag = "\"-c currstep=" + p.getCurrString()
            + ",n=" + p.getCurrString()
            + ",max_name=" + p.History.get(p.currstep).getMaxName() + "\"";
        Cmd = List.of(
                "crmodels2",
                "--cputime",
                "15",
                "--cr2opts",
                "\"--min-card\"",
                "--gopts",
                currentFlag,
                "1",
                filePathDomain,
                filePathHistory
        );

        System.out.println("legal subsequent obs command: ");
        for (String s : Cmd)
            System.out.print(s + " ");
        System.out.println();
    }

    /**
     * checks to ensure that the obs made by the user are legal
     * i.e. they are consistent.
     * if they are not legal, the user must change them until they are legal.
     * If the observations are legal then the number of unobserved exogenous
     * actions is parsed from answer set along with the observations and
     * both are stored in the history at currstep.
     */
    private void LegalSubsequentObs() throws IOException {
        obsLegal = true;
        InputStream is;
        Runtime rt;
        ProcessBuilder pb;
        Process po;
        BufferedReader br;
        rt = Runtime.getRuntime();

        p.setlabel("Interpreting Observations ... Please wait");

        pb = new ProcessBuilder(Cmd);
        po = pb.start();
        try {
            po.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        is = po.getInputStream();
        br = new BufferedReader(new InputStreamReader(is));
        String s, s2, s3;
        String[] stringTokens;
        int linelength;
        int k;
        s = br.readLine();

        if (s == null || s.startsWith("False")) {
            System.out.println("no models");
            obsLegal = false;
        } else {
            while (true) {

                s = br.readLine();
                // System.out.println("s " + s);
                if (s == null) {
                    System.out.println("no models");
                    break;
                } else {
                    // System.out.println(s);
                    stringTokens = s.split(" ");
                    linelength = stringTokens.length;
                    for (k = 0; k < linelength; k++) {
                        s2 = stringTokens[k];
                        // number_unobserved
                        if (s2.startsWith("number_unobserved(")) {
                            p.History.get(p.currstep).setnum_unob(s3 = IntentAgent.extractArg1(s2));
                            // System.out.println("number_unob " + s3);
                            System.out.println("number unob recorded at " + p.currstep + " is "
                                + p.History.get(p.currstep).getnum_unob());
                        }
                    }
                }
            }
            br.close();
            p.setlabel("");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jInternalFrame1 = new javax.swing.JInternalFrame();
        Close = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        SetTrue = new javax.swing.JButton();
        SetFalse = new javax.swing.JButton();
        ApplyCWA = new javax.swing.JButton();
        SetUnspec = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        HpdTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ObsTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        SetTrue1 = new javax.swing.JButton();
        SetFalse1 = new javax.swing.JButton();
        ApplyCWA1 = new javax.swing.JButton();
        SetUnspec2 = new javax.swing.JButton();
        backFromObs = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jInternalFrame1.setVisible(true);

        Close.setText("Finished making Observations");
        Close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CloseActionPerformed(evt);
            }
        });

        SetTrue.setText("Set True");
        SetTrue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetTrueActionPerformed(evt);
            }
        });

        SetFalse.setText("Set False");
        SetFalse.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetFalseActionPerformed(evt);
            }
        });

        ApplyCWA.setText("Apply CWA");
        ApplyCWA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ApplyCWAActionPerformed(evt);
            }
        });

        SetUnspec.setText("Set Unspecified");
        SetUnspec.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetUnspecActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(SetTrue)
                    .add(9, 9, 9)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(ApplyCWA)
                        .add(jPanel1Layout.createSequentialGroup()
                            .add(SetFalse)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(SetUnspec)))
                    .add(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel1Layout.createSequentialGroup()
                    .add(18, 18, 18)
                    .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel1Layout.createSequentialGroup()
                            .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(SetFalse)
                                .add(SetUnspec))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(ApplyCWA))
                        .add(SetTrue)))
        );

        HpdTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object[][]{
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String[]{
                "Name", "Observed value", "Expected value"
            }
        ));
        HpdTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                HpdTableMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(HpdTable);

        jLabel1.setText("Observations of occurrences of actions");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel2Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                .add(jPanel2Layout.createSequentialGroup()
                    .add(55, 55, 55)
                    .add(jLabel1)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(jPanel2Layout.createSequentialGroup()
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 388, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(0, 8, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel2Layout.createSequentialGroup()
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel1)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 287, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(12, 12, 12))
        );

        ObsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object[][]{
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String[]{
                "Name", "Observed value", "Expected value"
            }
        ));
        jScrollPane1.setViewportView(ObsTable);

        jLabel2.setText("Observations of values of fluents");

        SetTrue1.setText("Set True");
        SetTrue1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetTrue1ActionPerformed(evt);
            }
        });

        SetFalse1.setText("Set False");
        SetFalse1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetFalse1ActionPerformed(evt);
            }
        });

        ApplyCWA1.setText("Apply CWA");
        ApplyCWA1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ApplyCWA1ActionPerformed(evt);
            }
        });

        SetUnspec2.setText("Set Unspecified");
        SetUnspec2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SetUnspec2ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel4Layout.createSequentialGroup()
                    .add(28, 28, 28)
                    .add(SetTrue1)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(ApplyCWA1)
                        .add(jPanel4Layout.createSequentialGroup()
                            .add(SetFalse1)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(SetUnspec2)))
                    .add(0, 20, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel4Layout.createSequentialGroup()
                    .add(18, 18, 18)
                    .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(SetFalse1)
                        .add(SetUnspec2)
                        .add(SetTrue1))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 12, Short.MAX_VALUE)
                    .add(ApplyCWA1))
        );

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel3Layout.createSequentialGroup()
                    .add(jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(jPanel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel3Layout.createSequentialGroup()
                            .add(14, 14, 14)
                            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 393, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(15, Short.MAX_VALUE))
                .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel3Layout.createSequentialGroup()
                    .add(0, 0, Short.MAX_VALUE)
                    .add(jLabel2)
                    .add(99, 99, 99))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jLabel2)
                    .add(13, 13, 13)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 286, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        backFromObs.setText("go back");
        backFromObs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backFromObsActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jInternalFrame1Layout = new org.jdesktop.layout.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jInternalFrame1Layout.createSequentialGroup()
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .add(21, 21, 21)
                            .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(36, 36, 36)
                            .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .add(36, 36, 36)
                            .add(backFromObs)
                            .add(189, 189, 189)
                            .add(Close)))
                    .addContainerGap(22, Short.MAX_VALUE))
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                    .add(16, 16, 16)
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(18, 18, 18)
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(Close, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(backFromObs))
                    .add(30, 30, 30))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jInternalFrame1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(49, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(45, Short.MAX_VALUE)
                    .add(jInternalFrame1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SetFalseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetFalseActionPerformed
        int[] sl = HpdTable.getSelectedRows();
        // System.out.println("in set false " + max_required_hpd);
        int i;
        for (i = 0; i < sl.length && sl[i] < rHpd; i++) {
            HpdTable.setValueAt("false", sl[i], 1);
        }        // TODO add your handling code here:
    }//GEN-LAST:event_SetFalseActionPerformed

    private void CloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CloseActionPerformed

        // these are new observations, adds to ObsTable
        parseTable();
        try {
            // creates history that includes what new obs
            p.printHistoryFile();
            // p.printHistoryFileToScreen();
            LegalSubsequentObs();
            if (obsLegal)
                // calls solver with cmd on history file
                // return true if consistent false otherwis
                // close window ... initial obs are legal
                setVisible(false);
            else {
                p.History.get(p.currstep).clear_obs_hpd();

                JOptionPane.showMessageDialog(jInternalFrame1,
                    "Observations are not legal (i.e. they make the history inconsistent). "
                        + "Modify your observations ");
                // p.setStepOn(1);
                // p.setStepOff(2);
            }
            p.updateHistoryView();
            // begins with determine_ia
            p.AgentLoop();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ObservationsDialog.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_CloseActionPerformed

    private void SetTrueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetTrueActionPerformed
        // TODO add your handling code here:
        // System.out.println("in set true "+ max_required_hpd);
        int[] sl = HpdTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < rHpd; i++) {
            HpdTable.setValueAt("true", sl[i], 1);
        }
    }//GEN-LAST:event_SetTrueActionPerformed

    private void ApplyCWAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ApplyCWAActionPerformed

        // for (int i=0; i < p.physical_Fluents.size();i++)
        int i = 0;
        while (HpdTable.getModel().getValueAt(i, 0) != null) {
            if (HpdTable.getModel().getValueAt(i, 1) != "true")
                if (i >= max_required_hpd)
                    HpdTable.getModel().setValueAt("false", i, 1);
            i++;
        }
    }//GEN-LAST:event_ApplyCWAActionPerformed

    private void SetTrue1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetTrue1ActionPerformed
        int[] sl = ObsTable.getSelectedRows();
        // System.out.println("in set false obs " + max_required_obs);
        int i;
        for (i = 0; i < sl.length && sl[i] < rObs; i++) {
            ObsTable.setValueAt("true", sl[i], 1);
        }
    }//GEN-LAST:event_SetTrue1ActionPerformed

    private void SetFalse1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetFalse1ActionPerformed
        int[] sl = ObsTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < rObs; i++) {
            ObsTable.setValueAt("false", sl[i], 1);
        }
    }//GEN-LAST:event_SetFalse1ActionPerformed

    private void ApplyCWA1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ApplyCWA1ActionPerformed
        int i = 0;
        while (ObsTable.getModel().getValueAt(i, 0) != null) {
            if (ObsTable.getModel().getValueAt(i, 1) != "true")
                if (i >= max_required_obs) {
                    ObsTable.getModel().setValueAt("false", i, 1);
                }
            i++;
        }
    }//GEN-LAST:event_ApplyCWA1ActionPerformed

    private void HpdTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_HpdTableMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_HpdTableMouseClicked

    private void SetUnspecActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetUnspecActionPerformed
        int[] sl = HpdTable.getSelectedRows();
        // System.out.println("in set false "+max_required_hpd);
        int i;
        for (i = 0; i < sl.length && sl[i] < rHpd; i++) {
            HpdTable.setValueAt(null, sl[i], 1);
        }
    }//GEN-LAST:event_SetUnspecActionPerformed

    private void SetUnspec2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SetUnspec2ActionPerformed
        int[] sl = ObsTable.getSelectedRows();
        int i;
        for (i = 0; i < sl.length && sl[i] < rObs; i++) {
            ObsTable.setValueAt(null, sl[i], 1);
        }
    }//GEN-LAST:event_SetUnspec2ActionPerformed

    private void backFromObsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backFromObsActionPerformed

        p.backFromObsClicked();
        setVisible(false);

        // close the obs dialog

        // I go back to iterate, What needs to change,

        // 2014-01-29 Note:
        // I think I need to back to just before the iterate is clicked.
        // This means undoing everything in the iterateclicked method
        // and reseting the interface
        //
        //
        // History View is not updated properly
        // I think I will save the HPD an OBS string in AandO
        // The function call will be made in the obs dialog
        // I think the function will be alot like th updateHistoryView
        //
        // Then update historyView will just rewrite the entire table
        //
        // Similarly for projection??
        //
        //
        // Another problem

    }//GEN-LAST:event_backFromObsActionPerformed

    private void parseTable() {
        String name, value, expected;
        // parse the HpdTable and update hpd Arraylist in history at currstep

        // while(HpdTable.getModel().getValueAt(k,0)!=null){
        for (int k = 0; k < rHpd; k++) {
            name = (String) HpdTable.getModel().getValueAt(k, 0);
            value = (String) HpdTable.getModel().getValueAt(k, 1);
            expected = (String) HpdTable.getModel().getValueAt(k, 2);
            // System.out.println(name);

            if (name != null) {
                p.History.get(p.currstep).addHappenedNameValueExpected(name, value, expected);
                // System.out.println("addHappenedNameValueE "+ name +" "+ value + " "+expected);
                if (value != null) { // only if the observed value is not null is is added to hpd
                    // hpd populates the history file
                    // System.out.println("recording hpd "+name+" "+value +" "+p.currstep);
                    p.History.get(p.currstep).sethpd(name, value);
                    // System.out.println("sethpd "+ name +" "+ value);
                }
            }
            // k++;
        }
        // parse the ObsTable and update obs Arraylist in history at currstep
        // k =0;
        // while(ObsTable.getModel().getValueAt(k,0)!=null){
        for (int k = 0; k < rObs; k++) {
            name = (String) ObsTable.getModel().getValueAt(k, 0);
            value = (String) ObsTable.getModel().getValueAt(k, 1);
            expected = (String) ObsTable.getModel().getValueAt(k, 2);
            // System.out.println(name);

            if (name != null) {
                p.History.get(p.currstep).addObservedNameValueExpected(name, value, expected);
                // System.out.println("addObservedNameValueE "+ name +" "+ value + " "+expected);
                if (value != null) {  // only if the observed value is not null is is added to obs
                    // obs is populates the history file
                    // System.out.println("recording obs"+name+" "+value +" "+p.currstep);
                    p.History.get(p.currstep).setobs(name, value);
                    // System.out.println("setobs "+ name +" "+ value);
                }
            }
            // k++;
        }
    }

    // /**
    //  * @param args the command line arguments
    //  */
    // public static void main(String args[]) {
    //     // Set the Nimbus look and feel
    //     //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    //     // If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
    //     // For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
    //     try {
    //         for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
    //             if ("Nimbus".equals(info.getName())) {
    //                 javax.swing.UIManager.setLookAndFeel(info.getClassName());
    //                 break;
    //             }
    //         }
    //     } catch (ClassNotFoundException ex) {
    //         java.util.logging.Logger.getLogger(ObservationsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (InstantiationException ex) {
    //         java.util.logging.Logger.getLogger(ObservationsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (IllegalAccessException ex) {
    //         java.util.logging.Logger.getLogger(ObservationsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     } catch (javax.swing.UnsupportedLookAndFeelException ex) {
    //         java.util.logging.Logger.getLogger(ObservationsDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    //     }
    //     //</editor-fold>
    //
    //     // Create and display the dialog
    //     java.awt.EventQueue.invokeLater(new Runnable() {
    //         public void run() {
    //             ObservationsDialog dialog = new ObservationsDialog(IntentAgent, true);
    //             dialog.addWindowListener(new java.awt.event.WindowAdapter() {
    //                 @Override
    //                 public void windowClosing(java.awt.event.WindowEvent e) {
    //                     System.exit(0);
    //                 }
    //             });
    //             dialog.setVisible(true);
    //         }
    //     });
    // }
}
