/*
 * 2 feb 2014
 * Author: Justin Blount
 * Contact at justin.blount@gmail.com
 */
package com.gmail.justin_blount.intentionalaplmgr;

import java.util.ArrayList;

/**
 * @author justinblount
 */
public class AandO {
    // An ArrayList of AandO objects describe the history.
    private final int step;
    private final ArrayList<String> obs = new ArrayList<>();
    private final ArrayList<String> hpd = new ArrayList<>();
    // will need get length, get(k), add(s) for both arrays
    // a list of fluent that are col 0
    private final ArrayList<String> observed_names = new ArrayList<>();
    private final ArrayList<String> happened_names = new ArrayList<>();
    // a list of values that are col 1, are null or ""? if no observed values
    private final ArrayList<String> observed_values = new ArrayList<>();
    private final ArrayList<String> happened_values = new ArrayList<>();
    private final ArrayList<String> happened_expected = new ArrayList<>();
    private final ArrayList<String> observed_expected = new ArrayList<>();
    private final ArrayList<String> need_obs_goal = new ArrayList<>();
    private final ArrayList<String> need_obs_goal_expected_value = new ArrayList<>();
    private final ArrayList<String> need_obs_spec_exog_action = new ArrayList<>();
    private final ArrayList<String> projection = new ArrayList<>();
    private final ArrayList<String> explanation = new ArrayList<>();
    private final ArrayList<String> new_candidate = new ArrayList<>();
    private final ArrayList<String> obs_fluent = new ArrayList<>();
    private final ArrayList<String> obs_fluent_expected_value = new ArrayList<>();
    // data that is created when a obs dialog is created
    // see ObservationsDialog.java to see how they are used
    // I save them here, so I can use them after I come back to a dialog
    // denote the last line of the Obs,Hpd table that must be specified
    int max_required_hpd;
    // if a row above this is selected, (set unspec) button is disabled, otherwise is enabled
    int max_required_obs;
    int rObs;
    int rHpd;
    private String numb_unob;
    private int max_name;
    private String attmpt;
    private String intended_action;

    AandO(int k) {
        this.intended_action = "";
        step = k;
    }

    public int get_max_required_hpd() {
        return max_required_hpd;
    }

    public void set_max_required_hpd(int x) {
        max_required_hpd = x;
    }

    public int get_max_required_obs() {
        return max_required_obs;
    }

    public void set_max_required_obs(int x) {
        max_required_obs = x;
    }

    public int get_rObs() {
        return rObs;
    }

    public void set_rObs(int x) {
        rObs = x;
    }

    public int get_rHpd() {
        return rHpd;
    }

    public void set_rHpd(int x) {
        rHpd = x;
    }

    public int getobssize() {
        return obs.size();
    }

    public int gethpdsize() {
        return hpd.size();
    }

    public String getobs(int j) {
        return obs.get(j);
    }

    public void addObservedNameValue(String n, String v) {
        // the first argument of the obs(f,v,step)
        observed_names.add(n);
        observed_values.add(v);
    }

    public void addObservedNameValueExpected(String n, String v, String e) {
        // the first argument of the obs(f,v,step)
        observed_names.add(n);
        observed_values.add(v);
        observed_expected.add(e);
    }

    public String getObservedName(int j) {
        return observed_names.get(j);
    }

    public String getObservedValue(int j) {
        return observed_values.get(j);
    }

    public String getObservedExpected(int j) {
        return observed_expected.get(j);
    }

    public int observed_size() {
        return observed_names.size();
    }

    public void addHappenedNameValue(String n, String v) {
        // the first argument of the obs(f,v,step)
        happened_names.add(n);
        happened_values.add(v);
    }

    public void addHappenedNameValueExpected(String n, String v, String e) {
        // the first argument of the obs(f,v,step)
        happened_names.add(n);
        happened_values.add(v);
        happened_expected.add(e);
    }

    public String getHappenedName(int j) {
        return happened_names.get(j);
    }

    public String getHappenedValue(int j) {
        return happened_values.get(j);
    }

    public String getHappenedExpected(int j) {
        return happened_expected.get(j);
    }

    public int happened_size() {
        return happened_names.size();
    }

    public void clearObsHpd() {
        observed_names.clear();
        observed_values.clear();
        happened_names.clear();
        happened_values.clear();
    }

    public String getobsname(int j) {
        // the first argument of the obs(f,v,step)
        String s = obs.get(j);
        int o, c;
        o = s.indexOf("(");
        c = s.indexOf(",");
        return (s.substring(o + 1, c));
    }

    public String getobsvalue(int j) {
        String s = obs.get(j);
        int o, c;
        o = s.indexOf(",");
        c = s.lastIndexOf(",");
        return (s.substring(o + 1, c));
    }

    public String gethpd(int j) {
        return hpd.get(j);
    }

    public void setobs(String o, String v) {
        if (o != null && v != null) {
            String ofact = "obs(" + o + "," + v + "," + step + ").";
            obs.add(ofact);
        }
    }

    public void sethpd(String o, String v) {
        int s = step - 1;
        if (o != null && v != null) {
            String ofact = "hpd(" + o + "," + v + "," + s + ").";
            hpd.add(ofact);
        }
    }

    // public String gethpdname(int j) {
    //     // the first argument of the obs(f,v,step)
    //     String s = obs.get(j);
    //     int o, c;
    //     o = s.indexOf("(");
    //     c = s.indexOf(",");
    //     return (s.substring(o + 1, c));
    // }

    // public String gethpdvalue(int j) {
    //     String s = obs.get(j);
    //     int o, c;
    //     o = s.indexOf(",");
    //     c = s.lastIndexOf(",");
    //     return (s.substring(o + 1, c));
    // }

    public void setnum_unob(String string) {
        numb_unob = string;
    }

    String getnum_unob() {
        return (numb_unob);
    }

    String getIA() {
        return intended_action;
    }

    void setattmpt(int iaIndex) {
        attmpt = "attempt(" + intended_action + "," + step + ").";
    }

    String getattmpt() {
        return attmpt;
    }

    void clear_attmpt() {
        attmpt = null;
    }

    void add_obs_goal(String string) {
        need_obs_goal.add(string);
    }

    void add_obs_spec_exog_action(String string) {
        need_obs_spec_exog_action.add(string);
    }

    public int get_obs_goal_size() {
        return need_obs_goal.size();
    }

    public String get_obs_goal(int k) {
        return need_obs_goal.get(k);
    }

    public int get_obs_spec_exog_action_size() {
        return need_obs_spec_exog_action.size();
    }

    public String get_obs_spec_exog_action(int k) {
        return need_obs_spec_exog_action.get(k);
    }

    Object get_obs_selectable_goal(int k) {
        return IntentAgent.extractArgArity1(need_obs_spec_exog_action.get(k));
    }

    Object get_obs_goal_expected_value(int k) {
        return need_obs_goal_expected_value.get(k);
    }

    void add_obs_goal_expected_value(String string) {
        need_obs_goal_expected_value.add(string);
    }

    boolean containsObsGoal(String goal) {
        return need_obs_goal.contains(goal);
    }

    void clear_obs_hpd() {
        obs.clear();
        hpd.clear();
    }

    void add_obs_fluent(String s) {
        obs_fluent.add(s);
    }

    public int get_obs_fluent_size() {
        return obs_fluent.size();
    }

    public String get_obs_fluent(int k) {
        return obs_fluent.get(k);
    }

    void add_obs_fluent_expected_value(String s) {
        obs_fluent_expected_value.add(s);
    }

    Object get_obs_fluent_expected_value(int k) {
        return obs_fluent_expected_value.get(k);
    }

    int getMaxName() {
        return max_name;
    }

    void setMaxName(int name) {
        max_name = name;
    }

    String getIa() {
        return (intended_action);
    }

    public void setIa(String string) {
        intended_action = string;
    }

    public void clearIa() {
        intended_action = null;
    }

    public int getProjectedSize() {
        return projection.size();
    }

    public String getProjected(int j) {
        return projection.get(j);
    }

    public void addProjected(String o, String s) {
        // int s = step-1;
        if (o != null && s != null) {
            String ofact = "o(" + o + "," + s + ")";
            projection.add(ofact);
        }
    }

    public void clearProjected() {
        projection.clear();
    }

    public int getExplanationSize() {
        return explanation.size();
    }

    public String getExplanation(int j) {
        return explanation.get(j);
    }

    public void addExplanation(String o, String s) {
        // int s = step-1;
        if (o != null && s != null) {
            String ofact = "o(" + o + "," + s + ")";
            explanation.add(ofact);
        }
    }

    public void clearExplanation() {
        explanation.clear();
    }

    public int getNewCandidateSize() {
        return new_candidate.size();
    }

    public String getPartNewCandidate(int j) {
        return new_candidate.get(j);
    }

    public void addPartNewCandidate(String o) {
        int s = step - 1;
        if (o != null) {
            String ofact = o + ".";
            new_candidate.add(ofact);
        }
    }

    public void clearPartNewCandidate() {
        new_candidate.clear();
    }
}
