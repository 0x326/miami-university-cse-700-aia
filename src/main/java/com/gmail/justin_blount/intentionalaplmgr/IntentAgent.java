/*
 * 2 feb 2014
 * Author: Justin Blount
 * Contact at justin.blount@gmail.com
 */
package com.gmail.justin_blount.intentionalaplmgr;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author justinblount
 */
public class IntentAgent extends javax.swing.JFrame {
    /**
     * File Domain is the file chosen by the user. file "RoomsWorld.lp" is an example that is provided.
     */
    File Domain;
    // The ArrayLists will contain the actions, and fluents from the domain file
    // These ArrayLists are loaded in the initAgent function.
    ArrayList<String> physical_agent_Actions = new ArrayList<>();
    ArrayList<String> mental_agent_Actions = new ArrayList<>();
    String special_agent_action = "wait";
    ArrayList<String> special_exog_Actions = new ArrayList<>();
    ArrayList<String> physical_exogenous_Actions = new ArrayList<>();
    ArrayList<String> physical_Fluents = new ArrayList<>();
    ArrayList<String> mental_Fluents = new ArrayList<>();
    ArrayList<String> possible_Goals = new ArrayList<>();

    /**
     * variable for the current step, maximum step, ir, and length of max projection
     */
    int currstep = 0;
    // max step: step(0..n)
    int n = 0;
    // init_rel
    int init_rel;
    int max_projection;
    boolean backFromIterateClicked = false;

    /**
     * The History is described by an arrayList of AandO (abbreviation for
     * actions and observations) objects.  I refer to the ith AandO object as a slice
     * of the history at steo i. Esentially it consists of the observations of fluents
     * at step i, observations of occurrences of actions at i-1, and an attempt to
     * perform an agent action at i-1.
     */
    ArrayList<AandO> History = new ArrayList<>();

    /**
     * This file will hold the History and will be used along with the Domain file
     * when the answer set solver is called.
     */
    File HistoryFile = new File("historyfile.txt");

    // These two Vectors are used to display the History and Projections on the UI
    Vector<String> HistoryView = new Vector<>();
    Vector<String> TrajectoryView = new Vector<>();
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList CurrExpViewList;
    private javax.swing.JInternalFrame HistoryViewFrame;
    private javax.swing.JList HistoryViewList;
    private javax.swing.JComboBox IAComboBox;
    private javax.swing.JButton Iterate;
    private javax.swing.JInternalFrame ProjectionViewFrame;
    private javax.swing.JTextField TextExplanation;
    private javax.swing.JTextField TextProjection;
    private javax.swing.JButton backFromIterate;
    private javax.swing.JLabel intended_actionLabel;
    private javax.swing.JButton jButton3;
    private javax.swing.JInternalFrame jInternalFrame1;
    private javax.swing.JInternalFrame jInternalFrame2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JFileChooser jfileChooser;
    // End of variables declaration//GEN-END:variables

    /**
     * Here is the constructor. After the constructor, the next function that
     * occurs is the formWindowOpened function.
     */
    public IntentAgent() throws IOException {
        initComponents();
        boolean bool = HistoryFile.createNewFile();
        IAComboBox.setVisible(false);
        intended_actionLabel.setVisible(false);
        Iterate.setEnabled(false);
    }

    public static String extractArgArity1(String s) {
        int o, c;
        o = s.indexOf("(");
        c = s.lastIndexOf(")");
        return (s.substring(o + 1, c));
    }

    public static String extractArg1(String s) {
        int o, c;
        o = s.indexOf("(");
        c = s.indexOf(",");
        return (s.substring(o + 1, c));
    }

    /**
     * action from unobserved(action,1
     */
    public static String extractActionFromAttempt(String s) {
        int o, c;
        o = s.indexOf("(");
        c = s.lastIndexOf(",");
        return (s.substring(o + 1, c));
    }

    /**
     * action from unobserved(action,1
     */
    public static String extractNamefromObsHpd(String s) {
        int o, c, cm;
        String s1;

        o = s.indexOf("(");
        c = s.lastIndexOf(",");
        s1 = s.substring(o + 1, c);
        // System.out.println("substring s1 : "+s1);
        cm = s1.lastIndexOf(",");
        // System.out.println("substring : "+s1.substring(cm+1,s1.length()));
        if ("false".equals(s1.substring(cm + 1)))
            return ("-" + s1.substring(0, cm) + ", ");
        return (s1.substring(0, cm) + ", ");
    }

    public static void orderedArrayListAdd(ArrayList<String> v, String s) {
        int i;

        for (i = 0; i < v.size(); i++)
            if (s.compareTo(v.get(i)) <= 0) {
                v.add(i, s);
                return;
            }
        v.add(s);
    }

    /**
     * function Main just declares and instance of class IntentAgent
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Set the Nimbus look and feel
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IntentAgent.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IntentAgent.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IntentAgent.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IntentAgent.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        // Create and display the form
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("AIA Manager started.");
                    new IntentAgent().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jfileChooser = new javax.swing.JFileChooser();
        jInternalFrame2 = new javax.swing.JInternalFrame();
        jScrollPane4 = new javax.swing.JScrollPane();
        CurrExpViewList = new javax.swing.JList();
        HistoryViewFrame = new javax.swing.JInternalFrame();
        jScrollPane1 = new javax.swing.JScrollPane();
        HistoryViewList = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        ProjectionViewFrame = new javax.swing.JInternalFrame();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TextExplanation = new javax.swing.JTextField();
        TextProjection = new javax.swing.JTextField();
        jInternalFrame1 = new javax.swing.JInternalFrame();
        Iterate = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        IAComboBox = new javax.swing.JComboBox();
        intended_actionLabel = new javax.swing.JLabel();
        backFromIterate = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jfileChooser.setCurrentDirectory(new java.io.File(System.getProperty("user.dir")));
        jfileChooser.setDialogTitle("Select a Domain file:");

        jInternalFrame2.setTitle("Current states and explanations\n");
        jInternalFrame2.setVisible(true);

        CurrExpViewList.setFont(new java.awt.Font("Monospaced", 0, 10)); // NOI18N
        jScrollPane4.setViewportView(CurrExpViewList);

        org.jdesktop.layout.GroupLayout jInternalFrame2Layout = new org.jdesktop.layout.GroupLayout(jInternalFrame2.getContentPane());
        jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
        jInternalFrame2Layout.setHorizontalGroup(
            jInternalFrame2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jInternalFrame2Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 575, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(15, Short.MAX_VALUE))
        );
        jInternalFrame2Layout.setVerticalGroup(
            jInternalFrame2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jInternalFrame2Layout.createSequentialGroup()
                    .addContainerGap()
                    .add(jScrollPane4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AIA Agent Manager");
        setMinimumSize(new java.awt.Dimension(610, 650));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        HistoryViewFrame.setTitle("History up to current step 0");
        HistoryViewFrame.setVisible(true);

        jScrollPane1.setViewportView(HistoryViewList);

        HistoryViewFrame.setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout HistoryViewFrameLayout = new org.jdesktop.layout.GroupLayout(HistoryViewFrame.getContentPane());
        HistoryViewFrame.getContentPane().setLayout(HistoryViewFrameLayout);
        HistoryViewFrameLayout.setHorizontalGroup(
            HistoryViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(HistoryViewFrameLayout.createSequentialGroup()
                    .add(14, 14, 14)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 568, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(14, Short.MAX_VALUE))
        );
        HistoryViewFrameLayout.setVerticalGroup(
            HistoryViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(HistoryViewFrameLayout.createSequentialGroup()
                    .addContainerGap()
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                    .addContainerGap())
        );

        getContentPane().add(HistoryViewFrame, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 620, 220));

        ProjectionViewFrame.setTitle("Information from a model of the History  ");
        ProjectionViewFrame.setVisible(true);

        jLabel5.setText("Explantion: ");

        jLabel6.setText("Projection:");

        TextExplanation.setEditable(false);
        TextExplanation.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextExplanationActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout ProjectionViewFrameLayout = new org.jdesktop.layout.GroupLayout(ProjectionViewFrame.getContentPane());
        ProjectionViewFrame.getContentPane().setLayout(ProjectionViewFrameLayout);
        ProjectionViewFrameLayout.setHorizontalGroup(
            ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(ProjectionViewFrameLayout.createSequentialGroup()
                    .add(31, 31, 31)
                    .add(ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jLabel6)
                        .add(jLabel5))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(TextExplanation, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
                        .add(TextProjection))
                    .addContainerGap(15, Short.MAX_VALUE))
        );
        ProjectionViewFrameLayout.setVerticalGroup(
            ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(ProjectionViewFrameLayout.createSequentialGroup()
                    .add(8, 8, 8)
                    .add(ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(TextExplanation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel5))
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(ProjectionViewFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel6)
                        .add(TextProjection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(14, Short.MAX_VALUE))
        );

        getContentPane().add(ProjectionViewFrame, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 620, 130));

        jInternalFrame1.setTitle("An intended action of the History");
        jInternalFrame1.setVisible(true);

        Iterate.setText("<html>Go ahead and attempt <br> to perform intended action <html> ");
        Iterate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IterateActionPerformed(evt);
            }
        });

        jButton3.setText("Quit");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        IAComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IAComboBoxActionPerformed(evt);
            }
        });

        intended_actionLabel.setText("Intended action: ");

        backFromIterate.setText("<html> Go back to previous <br>  observations <html> ");
        backFromIterate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backFromIterateActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jInternalFrame1Layout = new org.jdesktop.layout.GroupLayout(jInternalFrame1.getContentPane());
        jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(jInternalFrame1Layout.createSequentialGroup()
                    .add(23, 23, 23)
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .add(backFromIterate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(Iterate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 222, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(18, 18, 18)
                            .add(jButton3)
                            .add(23, 23, 23))
                        .add(jInternalFrame1Layout.createSequentialGroup()
                            .add(intended_actionLabel)
                            .add(34, 34, 34)
                            .add(IAComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 152, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(281, Short.MAX_VALUE))))
        );
        jInternalFrame1Layout.setVerticalGroup(
            jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                .add(org.jdesktop.layout.GroupLayout.TRAILING, jInternalFrame1Layout.createSequentialGroup()
                    .add(5, 5, 5)
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(intended_actionLabel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(IAComboBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(18, 18, 18)
                    .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(jButton3)
                        .add(jInternalFrame1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(Iterate, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                            .add(backFromIterate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap())
        );

        getContentPane().add(jInternalFrame1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 420, 620, 150));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 20, -1, -1));

        jMenu1.setText("File");

        jMenuItem2.setText("Exit");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar2.add(jMenu1);

        jMenu2.setText("View");

        jMenuItem1.setText("Minimal pre-models ");
        jMenu2.add(jMenuItem1);

        jMenuBar2.add(jMenu2);

        setJMenuBar(jMenuBar2);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Adds an attempt to the history.
     * Updates the history on the UI.
     * Increments currstep.
     * Get the next observations at currstep.
     * updates history on the UI with new observations.
     * Called AgentLoop.
     */
    private void IterateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IterateActionPerformed
        System.out.println("iterate action performed");
        if (IAComboBox.getItemCount() > 0) {
            int iaIndex;
            iaIndex = IAComboBox.getSelectedIndex();
            History.get(currstep).setattmpt(iaIndex);
            HistoryView.add("Attempt " + currstep + ") " + StringofAttempt(currstep));
        } else HistoryView.add("Attempt " + currstep + ") ");

        HistoryViewList.setListData(HistoryView);

        currstep++;
        Iterate.setEnabled(false);
        IAComboBox.setVisible(false);
        intended_actionLabel.setVisible(false);

        // shows a dialog box to get observations from the user for the current_step
        // the dialog contains a table,contains 3 cols fluent/action, expected and observe values
        // updates the obs and hpd arraylists from the history at currstep
        // obs_dialog does the interpret step when checking for legal.
        // System.out.println("get obs");
        if (currstep > 0)
            new ObservationsDialog(this, true).setVisible(true);

        // moved to finished button clicked
        // updates user interface with new observations.
        // updateObsHistoryView();
        // begins with determine_ia
        // AgentLoop();

    }//GEN-LAST:event_IterateActionPerformed

    public String getCurrString() {
        return (Integer.toString(currstep));
    }

    public String getnString() {
        return (Integer.toString(n));
    }

    public String getnProjectionString() {
        return (Integer.toString(currstep + max_projection));
    }

    public String getIRString() {
        return (Integer.toString(init_rel));
    }

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * The function shows the user a filechooser dialog box.
     * The user must select a domain file (stored is variable "file").
     * File "RoomsWorld.lp" is provided as an example.
     * The next function that occurs is function InitAgent(file) which
     * gathers the name of fluent and actions from the domain file.  This information
     * is stored in the ArrayLists that are datamembers of this class.
     * Afther InitAgent, function InitialStep() is called.  This corrosponds to the
     * initial step of the Agent loop.
     */
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // int returnVal = jfileChooser.showMessageDialog(parent,
        //     "Please select a domain file in the following dialog window.\n\n" +
        //         "Agent Request",
        //     JOptionPane.INFORMATION_MESSAGE);

        // int returnVal = jfileChooser.showMessageDialog(parent,
        //     "Please make initial observations by selecting the row of the fluent " +
        //         "and clicking the \"set true\", \"false\" or \"unspecified\" buttons.\n\n" +
        //         "when you are done click the \"finished making observations\" button.",
        //     "Agent Request",
        //     JOptionPane.INFORMATION_MESSAGE);

        // int returnVal = jfileChooser.showMessageDialog(parent,
        //     "Please make observations by selecting the row of the action (on the left table) " +
        //         "or fluent (on the right table) " +
        //         "and clicking the \"set true\", \"false\" or \"unspecified\" buttons.\n\n" +
        //         "when you are done click the \"finished making observations\" button.",
        //     "Agent Request",
        //     JOptionPane.INFORMATION_MESSAGE);        *

        // int returnVal = jfileChooser.showMessageDialog(parent,
        //     "When you are done inspecting the history, explanations, and projection, " +
        //         "click the \"iterate\" button at the bottom right of the main window.",
        //     "Agent Request",
        //     JOptionPane.INFORMATION_MESSAGE);

        int returnVal = jfileChooser.showOpenDialog(this);
        System.out.println("Domain file dialog...");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = jfileChooser.getSelectedFile();
                System.out.println("file selected by user.");
                InitAgent(file);  // get actions,fluents,etc
                try {
                    System.out.println("Starting agent.");
                    InitialStep();   // the first step of the agent loop (first obs)
                } catch (InterruptedException ex) {
                    Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("File selection cancelled.");
            System.exit(0);
        }
    }//GEN-LAST:event_formWindowOpened

    private void IAComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IAComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IAComboBoxActionPerformed

    private void TextExplanationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextExplanationActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextExplanationActionPerformed

    private void backFromIterateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backFromIterateActionPerformed

        // removes old obs and hpd from history view
        removeLastElementView();
        undo_AgentLoop_determine_ia();

        backFromIterateClicked = true;
        if (currstep == 0)
            new InitialObsDialog(this, true).setVisible(true);
        else
            new ObservationsDialog(this, true).setVisible(true);
    }//GEN-LAST:event_backFromIterateActionPerformed

    void backFromObsClicked() {

        currstep--;
        System.out.println("Back from Obs clicked " + currstep);
        // removes attempt from HistoryView
        HistoryViewFrame.setTitle("History up to current step " + currstep);
        HistoryView.removeElementAt(HistoryView.lastIndexOf(HistoryView.lastElement()));
        // reload explanation and projection and intended action to interface
        updateIAView();

        // back in obs dialog where this is called, the dialog.setVisible(false);
    }

    private String Croplastcomma(String S) {
        int c;
        if (S.contains(",")) {
            c = S.lastIndexOf(",");
            return (S.substring(0, c));
        }
        return S;
    }

    /**
     * get middle arg of three arg
     *
     * @param s
     * @return
     */
    private String extractArg2of3(String s) {
        int o, c;
        o = s.indexOf(",");
        c = s.lastIndexOf(",");
        return (s.substring(o + 1, c));
    }

    private String extractlastArg(String s2) {
        int c, p;
        c = s2.lastIndexOf(",");
        p = s2.lastIndexOf(")");
        return (s2.substring(c + 1, p));
    }

    private String extractArg1Arity2(String s2) {
        int c, p;
        p = s2.indexOf("(");
        c = s2.lastIndexOf(",");
        return (s2.substring(p + 1, c));
    }

    public void setlabel(String s) {
        jLabel1.setText(s);
    }

    /**
     * Calls an answer set solver, clingo, and parses the answer set
     * to get the actions and fluents and possible goals from the domain description.
     * It also get the initial value of constant ir.
     */
    public void InitAgent(File Dfile) throws IOException {
        System.out.println("initializing agent and domain...");
        System.out.println("extracting fluents, actions, and possible goals...");
        Domain = Dfile;
        String filePathDomain = Dfile.getAbsolutePath();
        System.out.println("file : " + filePathDomain);
        String currentFlag = "\"-c currstep=" + getCurrString() + "\"";
        List<String> Cmd = List.of(
                "crmodels2",
                "--cputime",
                "15",
                "--cr2opts",
                "\"--min-card\"",
                "--gopts",
                currentFlag,
                "1",
                filePathDomain
        );

        for (String value : Cmd)
            System.out.print(value + " ");
        System.out.println();

        System.out.println("computing an answer set...");
        InputStream i, ie;
        Runtime r = Runtime.getRuntime();

        ProcessBuilder pb = new ProcessBuilder(Cmd);
        Process p1 = pb.start();
        try {
            p1.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("computation ended.");
        i = p1.getInputStream();
        ie = p1.getErrorStream();
        BufferedReader b;
        b = new BufferedReader(new InputStreamReader(i));
        String s, s2, s3;
        String[] stringTokens;
        int linelength;
        int k;

        // string s is a line from the output of the answer set solver
        // string s2 is a token (white space delimited)
        // string s3 is the first argument of s2
        while (true) {
            s = b.readLine();
            if (s == null)
                break;
            else if (s.startsWith("UNSATISFIABLE")) {
                System.out.println("no models");
                break;
            } else {
                // System.out.println(s);
                stringTokens = s.split(" ");
                linelength = stringTokens.length;
                for (k = 0; k < linelength; k++) {
                    s2 = stringTokens[k];
                    if (s2.startsWith("inert_fluent(status(") ||
                            s2.startsWith("inert_fluent(active_goal(") ||
                            s2.startsWith("inert_fluent(next_name(")) {
                        // mental fluent
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(mental_Fluents, s3);
                        // System.out.println("mental FLUENT: "+s3);
                    } else if (s2.startsWith("inert_fluent(")) {
                        // physical fluent
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(physical_Fluents, s3);
                        // System.out.println("physical FLUENT: "+s3);
                    } else if (s2.startsWith("ag_action(start(") || s2.startsWith("ag_action(stop(")) {
                        // mental agent_action
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(mental_agent_Actions, s3);
                        // System.out.println("mental ACTION: "+s3);
                    } else if (s2.startsWith("ag_action(")) {
                        // physical agent actions
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(physical_agent_Actions, s3);
                        // System.out.println("physical ACTION: "+s3);
                    } else if (s2.startsWith("exog_action(select(") ||
                            // special exog actions
                            s2.startsWith("exog_action(abandon(")) {
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(special_exog_Actions, s3);
                        // System.out.println("spec exog ACTION: "+s3);
                    } else if (s2.startsWith("exog_action(")) {
                        // mental fluent
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(physical_exogenous_Actions, s3);
                        // System.out.println("exog ACTION: "+s3);
                    } else if (s2.startsWith("poss_goal(")) {
                        s3 = extractArgArity1(s2);
                        orderedArrayListAdd(possible_Goals, s3);
                        // System.out.println("poss GOAL: "+s3);
                    } else if (s2.startsWith("init_rel(")) {
                        init_rel = Integer.parseInt(extractArgArity1(s2));
                        // System.out.println("init rel: "+init_rel);
                    } else if (s2.startsWith("max_projection(")) {
                        max_projection = Integer.parseInt(extractArgArity1(s2));
                        // System.out.println("max_projection: "+max_projection);
                    }
                }
            }
        }
        System.out.println("extraction completed.");

        b.close();
        setlabel("");
    }

    /**
     * the initial step of the agent loop is to Observe the world.
     * The initial observations of the world are of values of fluents at
     * step 0.  Subsequent observations at i include occurrences of actions and
     * records of attempts at i-1.
     * show the user a dialog to select the intial observations.
     * The dialog contains athe table contains 2 cols - physical fluents and observed value.
     * Updates the obs and hpd arraylists from the history at currstep 0.
     * The dialog ensures that the observations are legal.
     * After getting the inital observations it continues to the AgentLoop function.
     */
    public void InitialStep() throws IOException, InterruptedException {
        System.out.println("=== current time step: " + currstep + " ===");
        // creates slice at 0
        AandO slice = new AandO(currstep);
        // adds iniital slice to history
        History.add(currstep, slice);
        // sets max_name
        History.get(currstep).setMaxName(init_rel);

        if (currstep == 0)
            new InitialObsDialog(this, true).setVisible(true);
        // moved to finshed obs click
        // updateObsHistoryView();
        // AgentLoop();
    }

    private Object makeObj(final String item) {
        return new Object() {
            @Override
            public String toString() {
                return item;
            }
        };
    }

    /**
     * gets all answer set of domain + obs +hpd at currstep
     * with this updates the AandO at currstep.
     */
    private void interpret_obs() throws IOException {
        String filePathDomain, filePathHistory;
        filePathDomain = Domain.getAbsolutePath();
        filePathHistory = HistoryFile.getAbsolutePath();
        printHistoryFile();
        printHistoryFileToScreen();

        // get the from History
        String currentFlag;
        currentFlag = "\"-c currstep=" + getCurrString()
            + ",n=" + getCurrString()
            + ",max_name=" + History.get(currstep).getMaxName() + "\"";
        // what other??
        List<String> Cmd = List.of(
                "crmodels2",
                "--cputime",
                "15",
                "--cr2opts",
                "\"--min-card\"",
                "--gopts",
                currentFlag,
                "1",
                filePathDomain,
                filePathHistory
        );

        System.out.println("interpret command: ");
        for (String value : Cmd)
            System.out.print(value + " ");
        System.out.println();

        InputStream i;
        Runtime r = Runtime.getRuntime();
        ProcessBuilder pb = new ProcessBuilder(Cmd);
        Process p1 = pb.start();
        try {
            p1.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        i = p1.getInputStream();
        BufferedReader b = new BufferedReader(new InputStreamReader(i));
        String s, s2, s3;
        String[] stringTokens;
        int linelength;
        int k;

        // string s is a line from the output of the answer set solver
        // string s2 is a token (white space delimited)
        // string s3 is the first argument of s2
        while (true) {
            s = b.readLine();
            if (s == null)
                break;
            else if (s.startsWith("UNSATISFIABLE")) {
                System.out.println("no models");
                break;
            } else {
                // System.out.println(s);
                stringTokens = s.split(" ");
                linelength = stringTokens.length;
                for (k = 0; k < linelength; k++) {
                    s2 = stringTokens[k];
                    // number_unobserved
                    if (s2.startsWith("number_unobserved(")) {
                        History.get(currstep).setnum_unob(s3 = extractArg1(s2));
                        // System.out.println("number_unob "+s3);
                        System.out.println("number unob recorded at " + currstep + " is " + History.get(currstep).getnum_unob());
                    }
                }
            }
        }
        b.close();
        System.out.println("after get obs");
    }

    /**
     * gets 1 answer set
     * And parses the answer set.
     * See code below for complete explanation of what is parsed from the answer set
     */
    private void determine_ia() throws IOException, InterruptedException {

        String filePathDomain, filePathHistory;
        filePathDomain = Domain.getAbsolutePath();
        filePathHistory = HistoryFile.getAbsolutePath();
        printHistoryFileToScreen();
        String currentFlag = "\"-c currstep=" + getCurrString()
            + ",n=" + getnProjectionString()
            + ",max_name=" + History.get(currstep).getMaxName()
            + ",number=" + History.get(currstep).getnum_unob() + "\"";
        List<String> Cmd = List.of(
                "crmodels2",
                "--cputime",
                "30",
                "--cr2opts",
                "\"--min-card\"",
                "--gopts",
                currentFlag,
                "1",
                filePathDomain,
                filePathHistory
        );

        System.out.println("intended action command: ");
        for (String value : Cmd)
            System.out.print(value + " ");
        System.out.println();

        setlabel("Finding an intended action ... Please wait");
        InputStream i;
        Runtime r = Runtime.getRuntime();
        ProcessBuilder pb = new ProcessBuilder(Cmd);
        Process p1 = pb.start();

        try {
            p1.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(IntentAgent.class.getName()).log(Level.SEVERE, null, ex);
        }

        i = p1.getInputStream();
        BufferedReader b = new BufferedReader(new InputStreamReader(i));
        String s, s2, s3;
        String[] stringTokens;
        int linelength;
        int k;
        int curModelIndex = -1;
        int NewMaxName = 0;
        boolean new_candidate = false;

        while (true) {
            s = b.readLine();
            // System.out.println(" line s: "+s);
            if (s == null)
                break;
            else {
                // System.out.println(s);
                if (s.startsWith("UNSATISFIABLE")) {
                    System.out.println("no models");
                    break;
                } else {
                    // System.out.println("1");
                    stringTokens = s.split(" ");
                    linelength = stringTokens.length;
                    for (k = 0; k < linelength; k++) {
                        // System.out.println("2 "+linelength);
                        s2 = stringTokens[k];
                        if (s2.startsWith("Answer:")) {
                            // marks begining of new answer set
                            System.out.println("answer set found");
                        } else if (s2.startsWith("intended_action(")) {
                            // intnded action at currstep
                            History.get(currstep).setIa(extractArg1Arity2(s2));
                            // System.out.println("ia of model "+ History.get(currstep).getIa());
                        } else
                            // The SEA that we might observe at the next currstep
                            if (s2.startsWith("need_obs_spec_exog_action(")) {   // System.out.println("we have need obs spec exog");
                                History.get(currstep + 1).add_obs_spec_exog_action(s3 = extractArgArity1(s2));
                                // System.out.println("need_obs_special "+s3);
                            } else
                                // the goals that we must observe at the next currstep (currstep+1)
                                if (s2.startsWith("need_obs_goal_expected_value(")) {  // System.out.println("we have  need obs goal");
                                    if (!History.get(currstep + 1).containsObsGoal(extractArg1Arity2(s2))) {
                                        History.get(currstep + 1).add_obs_goal(extractArg1Arity2(s2));
                                        History.get(currstep + 1).add_obs_goal_expected_value(extractlastArg(s2));
                                        // System.out.println("need_obs_goal: name "+extractArg1Arity2(s2) + " ev "+ extractlastArg(s2) );
                                    }
                                }
                        // the fluents that may be observed at the next currstep (currstep+1
                        // it is phy fluents that are not goals that need to be observed
                        if (s2.startsWith("obs_fluent_expected_value(")) {
                            // expected_value(F,V
                            History.get(currstep + 1).add_obs_fluent(extractArg1Arity2(s2));
                            History.get(currstep + 1).add_obs_fluent_expected_value(extractlastArg(s2));
                            // System.out.println("need_obs_fluent: name "+extractArg1Arity2(s2) + " ev "+ extractlastArg(s2) );
                        }
                        if (s2.startsWith("next_name(")) {
                            // History.get(currstep+1).setMaxName(extractArg1(s2));
                            NewMaxName = Integer.parseInt(extractArg1(s2));
                            // System.out.println("new max Name: "+NewMaxName);
                        }
                        // parse explanation
                        // get explanation and add it to currmodelindes (just like IA)
                        if (s2.startsWith("unobserved(")) {
                            History.get(currstep).addExplanation(extractArg1Arity2(s2), extractlastArg(s2));
                            // System.out.println("we have an unob "+ extractArg1Arity2(s2)+" "+extractlastArg(s2));
                        }
                        // parse projection projected projected_occurrences
                        // this is ultimantly used to populate the UI
                        if (s2.startsWith("projected_occurrences(")) {
                            History.get(currstep).addProjected(extractArg1Arity2(s2), extractlastArg(s2));
                            // System.out.println("we have proj occur "+ extractArg1Arity2(s2)+" "+extractlastArg(s2));
                        }
                        // parse the new candidate activity that is created
                        if (s2.startsWith("new_cand(")) {
                            new_candidate = true;
                            History.get(currstep).addPartNewCandidate(extractArgArity1(s2));
                            // System.out.println("we have new candidate "+ extractArgArity1(s2));
                        }
                    }
                }
            }
            setlabel(" ");
        }
        // If there has been a new candidate found, then at the next current step
        // we will need max_name++ otherwise just max_name is sufficient
        if (new_candidate) {
            History.get(currstep + 1).setMaxName(NewMaxName + 1);
        } else {
            History.get(currstep + 1).setMaxName(NewMaxName);
        }

        b.close();
    }

    /**
     * print the history to a file so it can be used along with the domain file
     * in the call to clingo.
     */
    public void printHistoryFile() throws IOException {
        // System.out.println("printHistoryFile currstep "+currstep);
        FileWriter fw = new FileWriter(HistoryFile.getAbsoluteFile(), false);
        BufferedWriter bw = new BufferedWriter(fw);
        // System.out.println("from within print history File");
        for (AandO historyItem : History) {
            // System.out.println("printHistory at k: "+k);
            if (historyItem.getobssize() > 0) {
                for (int l = 0; l < historyItem.getobssize(); l++) {
                    bw.write(historyItem.getobs(l) + " ");
                    // System.out.println(History.get(k).getobs(l)+" ");
                }
                bw.newLine();
            }
            if (historyItem.gethpdsize() > 0) {
                for (int m = 0; m < historyItem.gethpdsize(); m++) {
                    bw.write(historyItem.gethpd(m) + " ");
                    // System.out.println(History.get(k).gethpd(m)+" ");
                }
                bw.newLine();
            }
            if (historyItem.getNewCandidateSize() > 0) {
                for (int i = 0; i < historyItem.getNewCandidateSize(); i++) {
                    bw.write(historyItem.getPartNewCandidate(i) + " ");
                    // System.out.println(History.get(k).getPartNewCandidate(i)+" ");
                }
            }
            if (historyItem.getattmpt() != null) {
                bw.write(historyItem.getattmpt() + " ");
                bw.newLine();
            }
        }
        bw.close();
    }

    public void printHistoryFileToScreen() throws IOException {
        System.out.println("History file at step " + currstep + " :");
        FileReader fr = new FileReader(HistoryFile.getAbsoluteFile());
        BufferedReader b = new BufferedReader(fr);
        String s;
        while ((s = b.readLine()) != null) {
            System.out.println(s);
        }
        b.close();
    }

    public void printHistory() {

        for (AandO historyItem : History) {
            if (historyItem.getobssize() > 0) {
                for (int l = 0; l < historyItem.getobssize(); l++)
                    System.out.print(historyItem.getobs(l) + " ");
                System.out.println();
            }
            if (historyItem.gethpdsize() > 0) {
                for (int m = 0; m < historyItem.gethpdsize(); m++)
                    System.out.print(historyItem.gethpd(m) + " ");
                System.out.println();
            }
            if (historyItem.getattmpt() != null) {
                System.out.print(historyItem.getattmpt() + " ");
            }
            if (historyItem.getNewCandidateSize() > 0) {
                for (int i = 0; i < historyItem.getobssize(); i++)
                    System.out.print(historyItem.getPartNewCandidate(i) + " ");
                System.out.println();
            }
        }
    }

    public void deletehistory() {
        // History.get(currstep);
    }

    /**
     * functions StringofObs,Hpd,Attempt,Explanations,Projections
     * create strings from the history and update the UI with these strings.
     */
    public String StringofObs(int step) {
        StringBuilder ObsString = new StringBuilder();
        // System.out.println("in string of obs " + History.get(step).getobssize() + " at "+step);
        for (int k = 0; k < History.get(step).getobssize(); k++) {
            // System.out.println("at the kth obs " + k);
            ObsString.append(extractNamefromObsHpd(History.get(step).getobs(k)));
        }
        ObsString = new StringBuilder(Croplastcomma(ObsString.toString()));
        return (ObsString.toString());
    }

    public String StringofHpd(int step) {
        StringBuilder HpdString = new StringBuilder();
        String s;
        int x;
        // System.out.println("in string of hpd " + History.get(step).gethpdsize() + " at "+step);
        for (int k = 0; k < History.get(step).gethpdsize(); k++) {
            // System.out.println("at the kth hpd " + k);
            // System.out.println("in string of hpd " + extractNamefromObsHpd(History.get(step).gethpd(k)));
            HpdString.append(extractNamefromObsHpd(History.get(step).gethpd(k)));
        }
        HpdString = new StringBuilder(Croplastcomma(HpdString.toString()));
        return (HpdString.toString());
    }

    private String StringofAttempt(int k) {
        String AttString = "";
        if (History.get(k).getattmpt() != null)
            AttString = extractActionFromAttempt(History.get(k).getattmpt());
        return AttString;
    }

    public String StringofExplanations(int step) {
        StringBuilder ExpString = new StringBuilder();
        String s;
        int x;
        // System.out.println("in string of explanation " + History.get(step).getExplanationSize() + " at "+step);
        if (History.get(step).getExplanationSize() > 0) {
            for (int k = 0; k < History.get(step).getExplanationSize(); k++) {
                // System.out.println("at the kth hpd " + k);
                // System.out.println("in string of hpd " + extractNamefromObsHpd(History.get(step).gethpd(k)));
                ExpString.append(History.get(step).getExplanation(k)).append(", ");
            }
            ExpString = new StringBuilder(Croplastcomma(ExpString.toString()));
        } else ExpString = new StringBuilder("none");
        // System.out.println("ExpString " + ExpString);
        return (ExpString.toString());
    }

    public String StringofProjection(int step) {
        StringBuilder ProjString = new StringBuilder();
        String s;
        int x;
        if (History.get(step).getProjectedSize() > 0) {
            for (int k = History.get(step).getProjectedSize(); k > 0; k--) {
                ProjString.append(History.get(step).getProjected(k - 1)).append(", ");
            }
            ProjString = new StringBuilder(Croplastcomma(ProjString.toString()));
        } else ProjString = new StringBuilder("none");
        return (ProjString.toString());
    }

    public void updateHistoryView() {
        // System.out.println("Update History View ");
        int c = currstep - 1;
        HistoryViewFrame.setTitle("History up to current step " + currstep);

        if (currstep == 0) {
            HistoryView.add("Obs " + currstep + ") " + StringofObs(currstep));
        } else {
            HistoryView.add("Hpd " + c + ") " + StringofHpd(currstep));
            HistoryView.add("Obs " + currstep + ") " + StringofObs(currstep));
        }
        HistoryViewList.setListData(HistoryView);
    }

    private void updateIAView() {
        TextExplanation.setEditable(true);
        TextExplanation.setText(StringofExplanations(currstep));
        TextExplanation.setEditable(false);

        TextProjection.setEditable(true);
        TextProjection.setText(StringofProjection(currstep));
        TextProjection.setEditable(false);

        intended_actionLabel.setText("Intended action at current step " + currstep + ":");
        intended_actionLabel.setVisible(true);
        IAComboBox.setVisible(true);
        IAComboBox.removeAllItems();
        IAComboBox.addItem(makeObj(History.get(currstep).getIA()));
        Iterate.setText("<html>Attempt to perform intended action<html>");
        Iterate.setEnabled(true);
    }

    /**
     * function is called after getting observations.
     * Note that the interpret observations step is done along with
     * checking the legality of the observations. This is done in the obs_dialog.
     * Adds another slice to the history at curstep+1.
     * Calls determine_ia which determines an intended action at currstep.
     * Updates UI with intended action.
     */
    public void AgentLoop() throws IOException, InterruptedException {
        AandO slice = new AandO(currstep + 1);  // create the next slice
        History.add(currstep + 1, slice);
        determine_ia();
        updateIAView();
    }

    void removeLastElementView() {
        System.out.println("remove last element");

        if (currstep == 0) {
            HistoryView.removeElementAt(HistoryView.lastIndexOf(HistoryView.lastElement()));
        } else {
            HistoryView.removeElementAt(HistoryView.lastIndexOf(HistoryView.lastElement()));
            HistoryView.removeElementAt(HistoryView.lastIndexOf(HistoryView.lastElement()));
        }
        HistoryViewList.setListData(HistoryView);

        TextExplanation.setEditable(true);
        TextExplanation.setText("");
        TextExplanation.setEditable(false);

        TextProjection.setEditable(true);
        TextProjection.setText("");
        TextProjection.setEditable(false);
    }

    private void undo_AgentLoop_determine_ia() {
        System.out.println("undo agent loop and ia");
        History.remove(currstep + 1);
        History.get(currstep).clearIa();
        History.get(currstep).clear_attmpt();
        History.get(currstep).clearExplanation();
        History.get(currstep).clearProjected();
        History.get(currstep).clearPartNewCandidate();
        Iterate.setEnabled(false);
        IAComboBox.removeAllItems();
        IAComboBox.setVisible(false);
        intended_actionLabel.setVisible(false);
    }
}
