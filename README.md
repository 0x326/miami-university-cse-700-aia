# Architecture for Intentional Agents

## Usage

```bash
./scripts/download_dependencies.sh ALL
./scripts/build_dependencies.sh ALL "${HOME}/.local"

INSTANCE_NAME=rooms_world
cat src/main/answer-set-programming/theory_of_intentions.lp \
    src/main/answer-set-programming/cr_prolog.lp \
    "src/main/answer-set-programming/${INSTANCE_NAME}_domain_encoding.lp" \
    "src/main/answer-set-programming/${INSTANCE_NAME}_instance.lp" \
    > domain_file.lp
./gradlew run
```
