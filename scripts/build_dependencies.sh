#!/usr/bin/env sh

usage() {
    echo "Usage: $0 PROGRAM [PREFIX]" >&2
    echo 'PROGRAM: (smodels|mkatoms|tbb|clasp|gringo|dlv_rsig|crmodels2|ALL)' >&2
    echo 'PREFIX: Install prefix (such as ${HOME}/.local)' >&2
    exit 1
}

configure() {
    if [ -x ./configure ]; then
        CONFIGURE_SCRIPT=configure
    elif [ -x ./configure.sh ]; then
        CONFIGURE_SCRIPT=configure.sh
    else
        echo 'configure script not found' >&2
        exit 1
    fi

    (
        if [ -n "${PREFIX}" ]; then
            echo "--prefix=${PREFIX}"
        fi
        if [ -n "${CFLAGS}" ] && [ -z "$1" ]; then
            echo "CFLAGS=${CFLAGS}"
        fi
        if [ -n "${CPPFLAGS}" ] && [ -z "$2" ]; then
            echo "CPPFLAGS=${CPPFLAGS}"
        fi
        if [ -n "${CXXFLAGS}" ] && [ -z "$3" ]; then
            echo "CXXFLAGS=${CXXFLAGS}"
        fi
        if [ -n "${LDFLAGS}" ] && [ -z "$4" ]; then
            echo "LDFLAGS=${LDFLAGS}"
        fi
    ) | xargs "./${CONFIGURE_SCRIPT}"
}

build_smodels() {
    echo 'Building Smodels' >&2
    cd smodels* || exit 1 \
        && chown "$(id -u):$(id -g)" . -R \
        && make -e --jobs "$(nproc)" \
        && if [ -z "${PREFIX}" ]; then
            make -e install
        else
            install -v smodels "${PREFIX}/bin"
        fi \
        && cd ..
}

build_mkatoms() {
    echo 'Building mkatoms' >&2
    cd mkatoms* || exit 1 \
        && chown "$(id -u):$(id -g)" . -R \
        && configure \
        && make all --jobs "$(nproc)" \
        && make install \
        && cd ..
}

build_tbb() {
    echo 'Building tbb' >&2
    if [ $((CLINGO_VERSION == 4)) -ne 0 ]; then
        cd tbb-4 || exit 1 \
            && make -e --jobs "$(nproc)" \
            && cd ..
    fi
}

build_clasp() {
    echo 'Building clasp' >&2
    if [ $((CLINGO_VERSION == 2)) -ne 0 ] || [ $((CLINGO_VERSION == 3)) -ne 0 ]; then
        cd clasp-1 || exit 1 \
            && sed -i 's/_exit(/_Exit(/' app/clasp_app.cpp \
            && configure 'no' 'no' '' '' \
            && cd build/release || exit 1 \
            && make --jobs "$(nproc)" \
            && make install \
            && cd ../.. \
            && cd ..
    elif [ $((CLINGO_VERSION == 4)) -ne 0 ]; then
        cd clasp-3 || exit 1 \
            && sed -i 's/#include <xlocale.h>/#include <locale.h>\n#include <time.h>/g' libprogram_opts/src/string_convert.cpp \
            && ( \
                if [ -z "${CLASP_3_MULTITHREADING}" ]; then \
                    configure 'no' 'no' '' '' \
                        && cd build/release || exit 1 \
                        && make --jobs "$(nproc)" \
                        && make --jobs "$(nproc)" install \
                        && cd ../.. \
                        ; \
                else \
                    configure "--with-mt=${CLASP_3_MULTITHREADING}" \
                        && cd build/release_mt || exit 1 \
                        && make --jobs "$(nproc)" \
                        && make --jobs "$(nproc)" install \
                        && cd ../.. \
                        ; \
                fi \
            ) \
            && cd ..
    elif [ $((CLINGO_VERSION == 5)) -ne 0 ]; then
        echo 'Clasp is installed in the clingo/gringo step' >&2
    else \
        echo "clingo version ${CLINGO_VERSION} is not supported" >&2
        exit 1
    fi
}

build_gringo() {
    echo 'Building gringo' >&2
    # clingo 2.x
    if [ $((CLINGO_VERSION == 2)) -ne 0 ]; then
        cd clingo-2 || exit 1 \
            && sed -i '1s/^/#include <stdlib.h>\n/' app/gringo_app.cpp \
            && sed -i 's/_exit(/_Exit(/' app/gringo_app.cpp \
            && make -e gringo_release clingo_release iclingo_release --jobs "$(nproc)" \
            && ln -sf "$(realpath build/gringo/release/bin/gringo)" "$(realpath build/clingo/release/bin/clingo)" "$(realpath build/iclingo/release/bin/clingo)" "${PREFIX:-/usr/local}/bin/" \
            && cd ..
    # clingo 3.x
    elif [ $((CLINGO_VERSION == 3)) -ne 0 ]; then
        cd clingo-3 || exit 1 \
            && make -e --jobs "$(nproc)" \
            && ln -sf "$(realpath build/release/bin/gringo)" "$(realpath build/release/bin/lemon)" "${PREFIX:-/usr/local}/bin/" \
            && cd ..
    # clingo 4.x
    elif [ $((CLINGO_VERSION == 4)) -ne 0 ]; then
        cd clingo-4 || exit 1 \
            && sed -i '1s/^/#include <math.h>\n/' libgringo/src/term.cc \
            && 2to3 -w SConscript \
            && scons --build-dir=release -j "$(nproc)" \
            && ln -sf "$(realpath build/release/gringo)" "$(realpath build/release/clingo)" "${PREFIX:-/usr/local}/bin/" \
            && cd ..
            # && scons --build-dir=release -j "$(nproc)" pyclingo \
            # && scons --build-dir=release -j "$(nproc)" luaclingo \
    # clingo 5.x
    elif [ $((CLINGO_VERSION == 5)) -ne 0 ]; then
        cd clingo-5 || exit 1 \
            && mkdir build \
            && cmake -e -S . -B build \
                -DCMAKE_BUILD_TYPE=Release \
                -DCMAKE_INSTALL_PREFIX="${PREFIX:-/usr/local}" \
            && cmake -e --build build --parallel "$(nproc)" \
            && cmake -e --install build \
            && cd ..
    else
        echo "clingo version ${CLINGO_VERSION} is not supported" >&2
        exit 1
    fi
}

build_dlv_rsig() {
    echo 'Building dlv_rsig' >&2
    cd dlv_rsig* || exit 1 \
        && chown "$(id -u):$(id -g)" . -R \
        && configure \
        && make all --jobs "$(nproc)" \
        && make install \
        && cd ..
}

build_crmodels2() {
    echo 'Building crmodels2' >&2
    cd crmodels* || exit 1 \
        && chown "$(id -u):$(id -g)" . -R \
        && if ! configure; then
            mv -v config.guess.latest config.guess \
                && mv -v config.sub.latest config.sub \
                configure
        fi \
        && sed -i '1s/^/#include "config.h"\n/' cr2.cc \
        && make all \
        && make install \
        && cd ..
}


if [ $(( $# == 0 )) -ne 0 ] || [ $(( $# > 3 )) -ne 0 ]; then
    usage
fi

PROGRAM=$1
PREFIX=$2

if [ -n "${PREFIX}" ]; then
    export CFLAGS="-I${PREFIX}/include"
    export CPPFLAGS="-I${PREFIX}/include"
    export CXXFLAGS="-I${PREFIX}/include"
    export LDFLAGS="-L${PREFIX}/lib"
fi

if [ "${PROGRAM}" = 'smodels' ]; then
    build_smodels
elif [ "${PROGRAM}" = 'mkatoms' ]; then
    build_mkatoms
elif [ "${PROGRAM}" = 'tbb' ]; then
    build_tbb
elif [ "${PROGRAM}" = 'clasp' ]; then
    build_clasp
elif [ "${PROGRAM}" = 'gringo' ]; then
    build_gringo
elif [ "${PROGRAM}" = 'dlv_rsig' ]; then
    build_dlv_rsig
elif [ "${PROGRAM}" = 'crmodels2' ]; then
    build_crmodels2
elif [ "${PROGRAM}" = 'ALL' ]; then
    build_smodels \
        && build_mkatoms \
        && build_tbb \
        && build_clasp \
        && build_gringo \
        && build_dlv_rsig \
        && build_crmodels2
else
    usage
fi
