FROM ubuntu:20.04 AS base

WORKDIR /root

ENV TZ=America/New_York

# Like ENV, but only available during build
ARG DEBIAN_FRONTEND=noninteractive

FROM base AS builder

# Dependencies
RUN \
    if command -v apk > /dev/null; then \
        apk add --no-cache \
            bison \
            boost-dev \
            build-base \
            cmake \
            cppunit-dev \
            doxygen \
            git \
            lua5.1-dev \
            lua5.3-dev \
            lua5.3-libs \
            mysql-dev \
            perl \
            python2-dev \
            python3-dev \
            re2c \
            scons \
            sqlite-dev \
            subversion \
            wget \
            ; \
    elif command -v apt-get > /dev/null; then \
        apt-get update \
            && ln -snf "/usr/share/zoneinfo/${TZ}" /etc/localtime \
            && echo "${TZ}" > /etc/timezone \
            && apt-get install --no-install-recommends -y \
                2to3 \
                bison \
                build-essential \
                ca-certificates \
                cmake \
                doxygen \
                git \
                gradle \
                libboost-all-dev \
                liblua5.3-dev \
                libmysql++-dev \
                libsqlite3-dev \
                openjdk-14-jdk \
                perl \
                pkg-config \
                python-is-python2 \
                python2-dev \
                python3-dev \
                re2c \
                scons \
                subversion \
                wget \
            && apt-get clean \
            && rm -rf /var/lib/apt/lists/* \
            ; \
    else \
        echo 'Package manager not detected' >&2 \
            && exit 1 \
            ; \
    fi

COPY scripts/download_dependencies.sh download_dependencies.sh

# Download Smodels
RUN ./download_dependencies.sh smodels

# Download mkatoms
RUN ./download_dependencies.sh mkatoms

# Download dlv_rsig
RUN ./download_dependencies.sh dlv_rsig

# Download CRModels2
RUN ./download_dependencies.sh crmodels

# Download Gradle
COPY gradle/wrapper /root/AIAmgr/gradle/wrapper
COPY gradlew /root/AIAmgr/

RUN cd /root/AIAmgr/ || exit 1 \
    && chmod u+x gradlew \
    && ./gradlew --version

ARG CLINGO_VERSION=3

# Download clingo
RUN ./download_dependencies.sh clingo

COPY scripts/build_dependencies.sh build_dependencies.sh

# Build Smodels
RUN ./build_dependencies.sh smodels

# Mkatoms
RUN ./build_dependencies.sh mkatoms

# tbb
RUN ./build_dependencies.sh tbb

# Set to 'std', 'tbb', or ''
ARG CLASP_3_MULTITHREADING=tbb

# clasp
RUN ./build_dependencies.sh clasp

# clingo/gringo
RUN ./build_dependencies.sh gringo

# dlv_rsig
RUN ./build_dependencies.sh dlv_rsig

# EZCSP

# CRModels2
RUN ./build_dependencies.sh crmodels2

# AIAmgr
COPY . /root/AIAmgr
RUN cd AIAmgr || exit 1 \
    && chmod u+x gradlew \
    && ./gradlew build

FROM builder as runner

WORKDIR /root/AIAmgr
CMD ["./gradlew", "run"]
