#!/usr/bin/env sh

usage() {
    echo "Usage: $0 PROGRAM" >&2
    echo 'PROGRAM: (smodels|mkatoms|dlv_rsig|crmodels2|clingo|ALL)' >&2
    exit 1
}

download_smodels() {
    wget 'http://www.tcs.hut.fi/Software/smodels/src/smodels-2.34.tar.gz' -O smodels.tar.gz \
        && tar xvzf smodels.tar.gz \
        && rm smodels.tar.gz \
        && cd smodels* || exit 1 \
        && wget 'http://www.tcs.hut.fi/Software/smodels/index.html' -O README.html \
        && cd ..
}

download_mkatoms() {
    wget 'http://www.mbal.tk/mkatoms/Source/mkatoms-2.18.tgz' -O mkatoms.tgz \
        && tar xvzf mkatoms.tgz \
        && rm mkatoms.tgz \
        && cd mkatoms* || exit 1 \
        && wget 'http://www.mbal.tk/mkatoms/README' \
        && wget 'http://www.mbal.tk/mkatoms/CHANGES' \
        && cd ..
}

download_dlv_rsig() {
    wget 'http://mbal.tk/ezcsp/dlv_rsig-1.8.11.tgz' -O dlv_rsig.tgz \
        && tar xvzf dlv_rsig.tgz \
        && rm dlv_rsig.tgz \
        && cd dlv_rsig* || exit 1 \
        && wget 'http://mbal.tk/ezcsp/index.html' -O README.html \
        && wget 'http://www.mbal.tk/ezcsp/RELEASES.txt' \
        && cd ..
}

download_crmodels2() {
    wget 'http://www.mbal.tk/crmodels/crmodels2-2.0.16.tgz' -O crmodels.tgz \
        && tar xvzf crmodels.tgz \
        && rm crmodels.tgz \
        && cd crmodels* || exit 1 \
        && wget 'http://www.mbal.tk/crmodels/README' \
        && wget 'http://www.mbal.tk/crmodels/RELEASES.txt' \
        && wget 'https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.guess;hb=HEAD' -O config.guess.latest \
        && wget 'https://git.savannah.gnu.org/gitweb/?p=config.git;a=blob_plain;f=config.sub;hb=HEAD' -O config.sub.latest \
        && chmod u+x config.guess.latest config.sub.latest \
        && cd ..
}

download_clingo() {
    # clingo 2.x
    if [ $((CLINGO_VERSION == 2)) -ne 0 ]; then
        svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/clasp-1.3.9' clasp-1 \
            && svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/gringo-2.0.5' clingo-2

    # clingo 3.x
    elif [ $((CLINGO_VERSION == 3)) -ne 0 ]; then
        svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/clasp-1.3.10' clasp-1 \
            && svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/gringo-3.0.5' clingo-3

    # clingo 4.x
    elif [ $((CLINGO_VERSION == 4)) -ne 0 ]; then
        git clone 'https://github.com/oneapi-src/oneTBB.git' -b 4.4.6 tbb-4 \
            && svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/clasp-3.2.0' clasp-3 \
            && svn checkout 'https://svn.code.sf.net/p/potassco/code/tags/gringo-4.5.4' clingo-4

    # clingo 5.x
    elif [ $((CLINGO_VERSION == 5)) -ne 0 ]; then
        git clone --recurse-submodules -b v5.4.1 'https://github.com/potassco/clingo.git' clingo-5

    else
        echo "clingo version ${CLINGO_VERSION} is not supported" >&2
        exit 1
    fi
}

if [ $(( $# != 1 )) -ne 0 ]; then
    usage
fi

PROGRAM=$1

if [ "${PROGRAM}" = 'smodels' ]; then
    download_smodels
elif [ "${PROGRAM}" = 'mkatoms' ]; then
    download_mkatoms
elif [ "${PROGRAM}" = 'dlv_rsig' ]; then
    download_dlv_rsig
elif [ "${PROGRAM}" = 'crmodels2' ]; then
    download_crmodels2
elif [ "${PROGRAM}" = 'clingo' ]; then
    download_clingo
elif [ "${PROGRAM}" = 'ALL' ]; then
    download_smodels \
        && download_mkatoms \
        && download_dlv_rsig \
        && download_crmodels2 \
        && download_clingo
else
    usage
fi
